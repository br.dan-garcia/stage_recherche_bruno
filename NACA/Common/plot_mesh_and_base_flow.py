import numpy as np
from antares import *
import matplotlib.tri as tri
import os
import glob
from scipy.spatial import cKDTree
import matplotlib.pyplot as plt

def LoadMesh(triname,ptsname):

    triangles  = np.loadtxt(triname)
    triangles -= 1
    pts        = np.loadtxt(ptsname)
    return triangles,pts


#list_ite1 = [int(x.split('solut_p_')[1].split('.dat')[0]) for x in glob.glob(os.path.join('solut_p_*.dat'))]
#list_ite = sorted(list_ite1)[-100:]
#print(list_ite)

meshtri1,meshpts1 = LoadMesh('connectivity.dat','coordinates.dat')
meshp1 = tri.Triangulation(meshpts1[:,0],meshpts1[:,1],meshtri1)




P1_base = Base()
P1_base['0000']=Zone()


for i in [0]:
    P1_base['0000']['%04s'%i]=Instant()
    P1_base[0]['%04s'%i]['x'] = meshp1.x
    P1_base[0]['%04s'%i]['y'] = meshp1.y
    P1_base[0]['%04s'%i]['z'] = 0.0*P1_base[0][0]['y']
    P1_base[0]['%04s'%i]['u'] = .5 + .5*(.5 + .5*np.tanh(.5*P1_base[0][0]['y']))
    P1_base[0]['%04s'%i].connectivity['tri'] = meshp1.triangles


writer = Writer('bin_tp')
writer['base'] = P1_base
writer['filename'] = 'base_flow.plt'
writer.dump()


# plotting

def plot_fem_mesh(nodes_x, nodes_y, elements):
    for element in elements:
        x = [nodes_x[element[i]] for i in range(len(element))]
        y = [nodes_y[element[i]] for i in range(len(element))]
        plt.fill(x, y, edgecolor='black', fill=False,linewidth=0.2)



nds_x = meshp1.x
nds_y = meshp1.y
nodal_values = .5+.5*(.5 + np.tanh(nds_y))


plot_fem_mesh(nds_x,nds_y,meshp1.triangles)

plt.tricontourf(meshp1,nodal_values)

plt.colorbar()
plt.axis()
ax = plt.gca()
ax.set_xlim([120, 180])
ax.set_ylim([-30, 30])
plt.savefig('base_flow.eps')
